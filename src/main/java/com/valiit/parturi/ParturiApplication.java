package com.valiit.parturi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParturiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParturiApplication.class, args);
	}

}
